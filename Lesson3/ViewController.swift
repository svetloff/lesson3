//
//  ViewController.swift
//  Lesson3
//
//  Created by Светлов Андрей on 21.08.2018.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        doAllTasks()
        
    }
    func doAllTasks() {
        //doTask0()
        //doTask1()
        //doTask2()
        //doTask3()
        //doTask4()
        doManhattan()
        poorStudent()
        richStudent()
        reverseNumber()
        
    }
    
    func doTask0 () {
        let number1 = randomNumberOfRange(minNumber: 0, maxNumber: 1000)
        let number2 = randomNumberOfRange(minNumber: 0, maxNumber: 1000)
        
        print("Task #0:")
        print("\t\t maximum of numbers (\(number1) , \(number2)) =", maxOfTwoNumbers(number1, number2))
        print()
    }
    
    func doTask1()  {
        let number = randomNumberOfRange(minNumber: 0, maxNumber: 10)
        
        print("Task #1:")
        print("\t\t square of number \(number): ", power(number, 2))
        print("\t\t cube of number \(number): ", power(number, 3))
        print()
    }
    
    func doTask2() {
        let number = randomNumberOfRange(minNumber: 0, maxNumber: 5)
        
        print("Task #2: number =", number)
        print()
        for i in 0...number {
            print("\t\t \(i)    \(number - i)")
            
        }
        print()
        
    }
    
    func doTask3() {
        //let number = randomNumberOfRange(minNumber: 2, maxNumber: 10)
        let number = 6
        
        var countDenominator = 0
        
        print("Task #3: number =", number)
        for i in 1..<number {
            if number%i == 0 {
                countDenominator += 1
            }
            
        }
        if countDenominator == 1 {
            print("\t\t total count denominators: \(countDenominator)")
        }
        else {
            print("\t\t total count denominators: \(countDenominator) (", terminator: "")
            for i in 1..<number {
                if number%i == 0 {
                    if countDenominator > 1 {
                        print(i, terminator: ", ")
                    }
                    else {
                        print(i, terminator: "")
                        
                    }
                    countDenominator -= 1
                    
                }
            }
            print(")", terminator: "")
        }
        print("\n", terminator: "\n")
    }
    
    func doTask4 () {
        //let number = randomNumberOfRange(minNumber: 2, maxNumber: 10000)
        let number = 100000
        
        print("Task #4: Range numbers up to", number)
        
        for i in 0..<number {
            
            if numberIsPerfect(i + 1) {
                print("\t\t Number \(i + 1) is PERFECT.")
                
            }
        }
        
        print()
        
    }
    
    func randomNumberOfRange(minNumber: UInt32, maxNumber: UInt32) -> Int {
        var result = 0
        
        result = Int(minNumber + arc4random()%(maxNumber - minNumber))
        
        return result
    }
    
    func maxOfTwoNumbers(_ number1: Int, _ number2: Int) -> Int {
        var result = number1
        
        if number1 < number2 {
            result = number2
        }
        
        return result
    }
    
    func power(_ number: Int, _ power: Int) -> Int {
        var result = 1
        
        for _ in 0..<power {
            result *= number
        }
        return result
    }
    
    func numberIsPerfect(_ number: Int) -> Bool {
        var result = false
        var sumDenominators = 0
        for i in 1..<number {
            if number%i == 0 {
                sumDenominators += i
            }
            
        }
        
        if number == sumDenominators {
            result = true
        }
        
        return result
    }
    
    func doManhattan () {
        print("Task #5: Manhattan")
        
        let yearOfStartDeposite = 1826
        let currentYear = 2018
        let percentDeposite = 6
        var sumSeposite = 26.0
        
        for _ in 0..<(currentYear - yearOfStartDeposite) {
            sumSeposite += sumDepositeOfOneYear(sumSeposite, percentDeposite)
        }
        
        print("\t\t Sum deposite:", roundNumber(sumSeposite, 2) )
        print()
    }
    
    func sumDepositeOfOneYear(_ sumDeposite: Double, _ percentDeposite: Int) -> Double {
        var result = sumDeposite
        
        result *= (Double(percentDeposite)/100)
        
        return result
    }
    
    func roundNumber(_ number: Double, _ rank: Int) -> Double {
        var result = number
        var doublePart = Int((number - Double(Int(number))) * Double(power(10, 3)))
        
        if doublePart%10 >= 5 {
           doublePart /= 10
           doublePart += 1
        }
        else {
           doublePart /= 10
        }
        
        result = Double(Int(number)) + Double(doublePart)/Double(power(10, 2))
        
        return result
    }
    
    func poorStudent() {
        let grant = 700
        var costInMonth = 1000.0
        let percentInflation = 3
        let monthsInSchoolYear = 10
        var sumCost: Double = 0
        
        print("Task #6: Poor student")
        
        for i in 0..<monthsInSchoolYear {
            if i == 0 {
                sumCost = costInMonth
            }
            else {
                costInMonth += percentOfNumber(costInMonth, Double(percentInflation))
                sumCost += costInMonth
            }
            
        }
        
        print("\t\t In the student's pocket should be \(Int(sumCost - Double(grant*monthsInSchoolYear)) + 1) UAH")
        print()
    }
    
    func percentOfNumber(_ number: Double, _ percent: Double) -> Double {
        var result = number
        
        result = result*(percent/100)
        
        return result
    }
    
    func richStudent() {
        let grant = 700
        var costInMonth = 1000.0
        let percentInflation = 3
        var countOfMonths = 0
        var moneyInPocket: Double = 2400
        
        print("Task #7: Rich student")
        
        while moneyInPocket > 0 {
            if countOfMonths > 0 {
                costInMonth += percentOfNumber(costInMonth, Double(percentInflation))
            }
            
            moneyInPocket = moneyInPocket + Double(grant) - costInMonth
            
            if moneyInPocket > 0 {
                countOfMonths += 1
            }
        }
        
        print("\t\t Money is enough for \(countOfMonths) months")
        print()
    }
    
    func reverseNumber() {
        var number = randomNumberOfRange(minNumber: 10, maxNumber: 1000)
        number = 1024
        var reverseNumber = 0
        
        print("Task #8: number ", number)
        
        while number / 10 >= 1 {
            reverseNumber = reverseNumber*10 + number%10
            number /= 10
        }
        
        reverseNumber = reverseNumber*10 + number
        
        print("\t\t reverse number:", reverseNumber)
    }
    
}



